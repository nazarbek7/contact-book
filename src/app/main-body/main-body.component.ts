import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Contact, SORT_BY_ALPHABETICALLY} from '../models/contact';

export type EditorType = 'address' | 'company' | 'post' | 'account-history';

@Component({
  selector: 'app-main-body',
  templateUrl: './main-body.component.html',
  styleUrls: ['./main-body.component.scss']
})
export class MainBodyComponent implements OnInit {
  selectedContact: Contact;

  baseUrl = 'http://demo.sibers.com';
  listOfContacts: any;
  editor: EditorType = 'address';
  localStorageContactsKey = 'sibers-contacts';

  isModalShowEditProfile = false;
  allContacts: Contact[] = [];

  constructor(private httpClient: HttpClient) {
  }

  // when showing modal menu link
  get showAddress() {
    return this.editor === 'address';
  }

  get showCompany() {
    return this.editor === 'company';
  }

  get showPost() {
    return this.editor === 'post';
  }

  get showAccountHistory() {
    return this.editor === 'account-history';
  }

  toggleEditor(type: EditorType) {
    this.editor = type;
  }

  // Searching
  searchContact(pattern) {
    this.listOfContacts = this.allContacts.filter((contact: Contact) => {
      return contact.name.toLowerCase().indexOf(pattern.toLowerCase()) >= 0;
    });
  }

  ngOnInit() {
    const contacts = localStorage.getItem(this.localStorageContactsKey); // get data from url
    if (contacts === null) {
      this.httpClient.get(this.baseUrl + '/users').subscribe((data: Contact[]) => {
        localStorage.setItem(this.localStorageContactsKey, JSON.stringify(data));
        this.listOfContacts = data;
        this.allContacts = data;
        console.log('retrieved from server');
      });
    } else {
      this.listOfContacts = JSON.parse(contacts);   // else data exist in local storage doing this code
      this.allContacts = this.listOfContacts;
      console.log('retrieved from localStorage');
    }
  }

  // Edit Profile
  saveEdit() {
    const contacts = JSON.parse(localStorage.getItem(this.localStorageContactsKey));
    const foundIndex = contacts.findIndex(x => x.id === this.selectedContact.id);
    contacts[foundIndex] = this.selectedContact;
    localStorage.setItem(this.localStorageContactsKey, JSON.stringify(contacts));
    this.closeModal();
  }

  // Getting data
  getProducts(user) {
    this.selectedContact = user;
    this.isModalShowEditProfile = !this.isModalShowEditProfile; // open modal
  }

  // Close Modal(Popup)
  closeModal() {
    this.isModalShowEditProfile = false;
  }

  // Sorting ALPHABETICALLY & GROUP
  sort(sortType: string) {
    console.log(sortType);
    if (sortType === SORT_BY_ALPHABETICALLY) {
      this.listOfContacts = this.listOfContacts.sort((contactA: Contact, contactB: Contact) => {
        if (contactA.name < contactB.name) {
          return -1;
        }
        if (contactA.name > contactB.name) {
          return 1;
        }
        return 0;
      });
    } else {
      this.listOfContacts = this.listOfContacts.sort((contactA: Contact, contactB: Contact) => {
        if (contactA.name < contactB.name) {
          return -1;
        }
        if (contactA.name > contactB.name) {
          return 1;
        }
        return 0;
      });
    }


    console.log(this.listOfContacts);
  }
}
