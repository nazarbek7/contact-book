interface Address {
    streetA: string;
    streetB: string;
    streetC: string;
    streetD: string;
    city: string;
    state: string;
    country: string;
    zipcode: number;
    geo: {
      lat: number,
      lng: number
    };
}

export class Contact {
  static id: any;
  name: string;
  username: string;
  email: string;
  address: Address;

  phone: number;

  website: string;

  company: {
    name: string,
    catchPhrase: string,
    bs: string
  };

  posts: [
    {
      words: Array<string>,
      sentence: string,
      sentences: string,
      paragraph: string
    },
    {
      words: Array<string>,
      sentence: string,
      sentences: string,
      paragraph: string
    },
    {
      words: Array<string>,
      sentence: string,
      sentences: string,
      paragraph: string
    }
  ];

  accountHistory: [
    {
      amount: number,
      date: Date,
      business: string,
      name: string,
      type: string,
      account: number
    },
    {
      amount: number,
      date: Date,
      business: string,
      name: string,
      type: string,
      account: number
    },
    {
      amount: string,
      date: Date,
      business: string,
      name: string,
      type: string,
      account: number
    }
  ];

  favorite: boolean;

  avatar: string;

  id: number;
}

export const SORT_BY_ALPHABETICALLY = 'alphabetically';
export const SORT_BY_GROUP = 'group';
