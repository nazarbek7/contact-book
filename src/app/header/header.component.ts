import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {SORT_BY_ALPHABETICALLY, SORT_BY_GROUP} from '../models/contact';

// @ts-ignore
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  pattern = '';
  sortingType = 'alphabetically';

  ALPHA = SORT_BY_ALPHABETICALLY;
  GROUP = SORT_BY_GROUP;

  @ViewChild('navBurger', {static: true}) navBurger: ElementRef;  // toggle menu(Burger Navbar menu bulma)
  @ViewChild('navMenu', {static: true}) navMenu: ElementRef;

  @Output() searchPattern: EventEmitter<string> = new EventEmitter();
  @Output() sortType: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  // toggle Navbar menu
  toggleNavbar() {
    this.navBurger.nativeElement.classList.toggle('is-active');
    this.navMenu.nativeElement.classList.toggle('is-active');
  }

  sort() {
    this.sortType.emit(this.sortingType);
  }

  search() {
    this.searchPattern.emit(this.pattern);
  }

}
